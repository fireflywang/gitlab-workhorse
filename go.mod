module gitlab.com/gitlab-org/gitlab-workhorse

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/FZambia/sentinel v1.0.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/getsentry/raven-go v0.1.2
	github.com/golang/gddo v0.0.0-20190419222130-af0f2af80721
	github.com/golang/protobuf v1.3.1
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/gorilla/websocket v1.4.0
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/jfbus/httprs v0.0.0-20190827093123-b0af8319bb15
	github.com/jpillora/backoff v0.0.0-20170918002102-8eab2debe79d
	github.com/prometheus/client_golang v1.0.0
	github.com/rafaeljusto/redigomock v0.0.0-20190202135759-257e089e14a1
	github.com/sebest/xff v0.0.0-20160910043805-6c115e0ffa35
	github.com/sirupsen/logrus v1.3.0
	github.com/stretchr/testify v1.3.0
	gitlab.com/gitlab-org/gitaly v1.57.0
	gitlab.com/gitlab-org/labkit v0.0.0-20190731061835-905271af7abb
	golang.org/x/lint v0.0.0-20180702182130-06c8688daad7
	golang.org/x/net v0.0.0-20190613194153-d28f0bde5980
	golang.org/x/tools v0.0.0-20190621195816-6e04913cbbac
	google.golang.org/grpc v1.16.0
	honnef.co/go/tools v0.0.1-2019.2.2
)
